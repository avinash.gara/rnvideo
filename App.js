/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
} from 'react-native';

import {
  Header,
  LearnMoreLinks,
  Colors,
  DebugInstructions,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';
import Video from 'react-native-video';

const App: () => React$Node = () => {
  return (
    <>
      <StatusBar barStyle="dark-content" />
      <SafeAreaView>
     <ScrollView>
             <View style={{flexDirection:'row' }}>
             <Video 
             style={{height: 200, width: '100%'}}
             fullscreenOrientation="all"
             source={{ uri: 'http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/BigBuckBunny.mp4' }}
             resizeMode="contain"
             rate={1}
             volume={1}
             fullscreen={true}
             repeat={false}
             controls={true}
             />
             </View>
             <View style={{flexDirection: 'row', margin: 20}}>
             <Text>checking</Text>
             </View>
             <View style={{flexDirection:'row' }}>
             <Video 
             style={{height: 200, width: '100%'}}
             fullscreenOrientation="all"
             source={{ uri: 'http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/BigBuckBunny.mp4' }}
             resizeMode="contain"
             rate={1}
             volume={1}
             fullscreen={true}
             repeat={false}
             controls={true}
             />
             </View>
             <View style={{flexDirection: 'row', margin: 20}}>
             <Text>checking</Text>
             </View>
             <View style={{flexDirection:'row' }}>
             <Video 
             style={{height: 200, width: '100%'}}
             fullscreenOrientation="all"
             source={{ uri: 'http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/BigBuckBunny.mp4' }}
             resizeMode="contain"
             volume={1}
             fullscreen={true}
             controls={true}
             />
             </View>
             <View style={{flexDirection: 'row', margin: 20}}>
             <Text>checking</Text>
             </View>
             <View style={{flexDirection:'row' }}>
             <Video 
             style={{height: 200, width: '100%'}}
             fullscreenOrientation="all"
             source={{ uri: 'http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/BigBuckBunny.mp4' }}
             resizeMode="contain"
             volume={1}
             fullscreen={true}
             controls={true}
             />
             </View>
             <View style={{flexDirection: 'row', margin: 20}}>
             <Text>checking</Text>
             </View>
             </ScrollView>
      </SafeAreaView>
    </>
  );
};

const styles = StyleSheet.create({

  backgroundVideo: {
    position: 'absolute',
    top: 0,
    left: 0,
    bottom: 0,
    right: 0,
  },

});

export default App;
